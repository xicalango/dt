#!/usr/bin/env sh

set -e

cmd="$@"

cmd_sha=$(echo "${cmd}" | shasum -a 256 | cut -d' ' -f1)

DT_DIR=${DT_DIR:-${TMPDIR:-/tmp}/dt-$(id -g)}

socket_file="${DT_DIR}/${cmd_sha}"

mkdir -p "${DT_DIR}"

if [ -S "${socket_file}" ]; then
  dtach -a "${socket_file}"
else
  dtach -n "${socket_file}" $@
  echo $cmd_sha
fi


